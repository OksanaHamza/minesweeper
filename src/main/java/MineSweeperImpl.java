import com.google.common.base.Strings;

/**
 * Created by oksana on 14.06.16.
 */
public class MineSweeperImpl implements MineSweeper {

    private char[][] field;
    private int m;
    private int n;

    @Override
    public void setMineField(String mineField) throws IllegalArgumentException {
        if (Strings.isNullOrEmpty(mineField)) {
            throw new IllegalArgumentException("MineField string should not be null or empty");
        }
        n = mineField.indexOf('\n');

        if (n == -1) throw new IllegalArgumentException("Incorrect string format");

        if(mineField.charAt(mineField.length()-1) == '\n') throw new IllegalArgumentException("Incorrect string format");

        for (int i = n; i < mineField.length(); i = i + n + 1) {
            if (mineField.charAt(i) != '\n') throw new IllegalArgumentException("Incorrect string format");
        }

        m = mineField.lastIndexOf('\n') / n + 1;

        for (int i = n; i < mineField.length(); i += n) {
            mineField = mineField.replaceFirst("\\n", "");
        }

        if (mineField.contains("\n")) throw new IllegalArgumentException("Incorrect string format");

        field = new char[m][n];
        int count = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                field[i][j] = mineField.charAt(count);
                count++;
            }
        }

    }

    public char[][] getField(){
        return field;
    }

    @Override
    public String getHintField() throws IllegalStateException {
        if (field == null) {
            throw new IllegalStateException("Mine-fields has not been set");
        }

        int[][] hintField = new int[m][n];
        hintField = calculateAdjacentMines(hintField);
        return fieldToString(hintField);
    }

    private int[][] calculateAdjacentMines(int[][] hintIntField) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (field[i][j] == '*') {
                    hintIntField[i][j] = -10;

                    if (j + 1 < n) hintIntField[i][j + 1]++;
                    if (j - 1 >= 0) hintIntField[i][j - 1]++;

                    if (i + 1 < m) {
                        hintIntField[i + 1][j] += 1;
                        if (j + 1 < n) hintIntField[i + 1][j + 1]++;
                        if (j - 1 >= 0) hintIntField[i + 1][j - 1]++;
                    }

                    if (i - 1 >= 0) {
                        hintIntField[i - 1][j] += 1;
                        if (j + 1 < n) hintIntField[i - 1][j + 1]++;
                        if (j - 1 >= 0) hintIntField[i - 1][j - 1]++;
                    }
                }
            }
        }

        return hintIntField;
    }

    private String fieldToString(int[][] hintIntField) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (hintIntField[i][j] >= 0) {
                    sb.append(hintIntField[i][j]);
                } else {
                    sb.append("*");
                }
            }
            sb.append("\n");
        }
        sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }

}
