import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by oksana on 16.06.16.
 */
public class MineSweeperTest {
    static MineSweeperImpl m;

    @BeforeClass
    public static void setUp() {
        m = new MineSweeperImpl();
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyStringIsIllegalArgument() {
        String s = "";
        m.setMineField(s);
    }

    @Test(expected = IllegalArgumentException.class)
    public void oneLineFieldIsIllegalArgument() {
        String s = "....*....";
        m.setMineField(s);
    }

    @Test(expected = IllegalArgumentException.class)
    public void newLineSymbolsShouldBeSpacedEvenly() {
        String s = "...\n..\n....";
        m.setMineField(s);
    }

    @Test(expected = IllegalArgumentException.class)
    public void fieldStringShouldNotContainExtraNewLineSymbols() {
        String s = "....\n....\n....\n..\n..";
        m.setMineField(s);
    }

    @Test(expected = IllegalArgumentException.class)
    public void fieldStringShouldNotContainNewLineSymbolInTheEnd() {
        String s = "....\n....\n....\n....\n";
        m.setMineField(s);
    }

    @Test
    public void setMineFieldShouldSetFieldIfInputCorrect() {
        String s = "....\n....\n....";
        m.setMineField(s);
        assertNotNull(m.getField());
    }

    @Test
    public void setMineFieldShouldConvertInputToCharArray() {
        String s = "...\n.*.";
        m.setMineField(s);
        char[][] ch = new char[][]{
                {'.', '.', '.'},
                {'.', '*', '.'}
        };
        assertArrayEquals(m.getField(), ch);
    }

    @Test
    public void getHintTest() {
        String field = "...\n..*\n.*.\n..*";
        String hint = "011\n12*\n1*3\n12*";
        m.setMineField(field);
        assertEquals(hint, m.getHintField());
    }
}
